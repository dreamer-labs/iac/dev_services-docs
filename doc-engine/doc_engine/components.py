import os
import yaml

import jinja2

from attrdict import AttrDict

from doc_engine.autodoc import AutoDoc
from doc_engine.generic import Generic
from doc_engine.raw_file import RawFile
from doc_engine.defaults import Templates
from doc_engine.rst_helpers import RSTHelpers
from doc_engine.util import resolve_path


class FileInfo(AttrDict):

    def __init__(self, component_name, item, src_dir=None, dst_dir=None):

        self._component_name = component_name
        self._src_dir = src_dir
        self._dst_dir = dst_dir

        if isinstance(item, str):
            item = {'name': item if '/' not in item else os.path.basename(item),
                    'path': item}

        super().__init__(item)

        if self._whatami() == 'autodoc':
            self._normalize_file_info()
            self._normalize_autodoc()
        else:
            try:
                self._normalize_file_info()
            except Exception:
                pass

    def _normalize_file_info(self):

        self['component'] = self._component_name

        if 'path' not in self:
            self['path'] = self.name

        self._normalize_name()

        if 'type' not in self:
            basename, ext = os.path.splitext(self.path)
            if ext in ('.md', '.rst'):
                self.type = ext.strip('.')
            elif ext in ('.py', '.png'):
                self.type = 'raw'
            else:
                self.type = 'text'

        if self['type'] not in ('inline',):
            self['src_paths'] = [os.path.join(self._src_dir, self.path)]
        else:
            self['src_paths'] = []

        self['dst_paths'] = []

        if 'highlight' not in self:
            self.highlight = 'text'

        if self.type == 'autodoc':
            self._normalize_autodoc()
        elif self.type == 'text':
            dictish(self, ('description', 'highlight'))
        elif self.type == 'openapi':
            dictish(self, ('description'))
        elif self.type == 'inline':
            dictish(self, ('content'))

        dictish(self, ('has_jinja', 'values', 'hide_toc', 'add_toc', 'add_indexes'))
        self['vals'] = self['values']
        del self['values']

        if not self['vals']:
            self['vals'] = {}

    def _normalize_name(self):
        if os.path.extsep in self.name:
            self['name'] = os.path.splitext(self.name)[0]

    def _normalize_autodoc(self):

        if 'modules' in self:

            self['modules'] = listish(self['modules'])
            modules = []
            for module in self['modules']:
                module = dictish(module, ('module', 'description',
                                          'usage', 'imports'))
                if module['imports']:
                    module['imports'] = listish(module['imports'])
                modules.append(module)

            self['modules'] = modules

        if 'imports' in self:
            self['imports'] = listish(self['imports'])

        if 'programs' in self:
            self['programs'] = listish(self['programs'])

    def _whatami(self):
        if 'name' in self and 'path' in self:
            return 'file_info'
        elif 'module' in self:
            return 'moudle'
        elif 'entrypoint' in self or 'parser_type' in self:
            return 'program'
        elif 'data' in self and 'heading' in self:
            return 'data'
        else:
            return 'generic'


class Files(list):

    def __init__(self, component, files, src_dir, dst_dir):
        super().__init__([FileInfo(component, file,
                                   src_dir, dst_dir) for file in files])

    def append(self, file_info):
        self.files.append(FileInfo(file_info))


def build_components(components, buld_dir, template_dir, values={}):

    clean_list = []
    doc_files = []
    requirements = []
    for component in components:

        if component['source_dir']:

            if component.get('site_root'):
                # site_root component location at buld_dir
                component['dest_dir'] = buld_dir
            else:
                component['dest_dir'] = os.path.join(buld_dir, component['name'])

            if build_target(component['dest_dir']):
                clean_list.append(component['dest_dir'])

            if not component['files']:
                component['files'] = ['README.md', 'README.rst']

            files = Files(component['name'],
                          component['files'] if 'files' in component else [],
                          component['source_dir'],
                          component['dest_dir'])

            component['docs'], comp_reqs = gen_component(
                component['source_dir'],
                component['dest_dir'],
                files,
                uri=component['uri'],
                version_info=component['version_info'],
                template_dir=template_dir,
                values=values)
            doc_files.extend(component['docs'])

        if 'requirements' in component:
            requirements.extend(listish(component['requirements']))

        if len(comp_reqs):
            requirements.extend(comp_reqs)

    return clean_list, doc_files, requirements


def build_target(dest_dir):

    if os.path.isdir(dest_dir):
        return False
    else:
        os.makedirs(dest_dir)
        return True


def gen_component(src_dir, dst_dir, files=None, uri=None,
                  version_info=None, template_dir=None, values={}):
    """Copy README files to the doc source tree for each componant.
    :src_dir str: Path to search for readme files
    :dst_dir str: Path to copy readme files to
    :files list: The list of the component's doc section
    :template_dir str: A path to the template_dir
    :uri str: The URI of the source component, used for external link back to project
    :return: (list, list)
    """
    template_dir = resolve_path(template_dir)
    loader = jinja2.FileSystemLoader(searchpath=template_dir)
    template_env = jinja2.Environment(loader=loader)
    template_env.lstrip_blocks = True
    template_env.trim_blocks = True
    doc_files = []
    requirements = []

    # table of contents
    hide_toc = [info.name for info in files if info.hide_toc]
    toc = sorted([info['name'] for info in files if info['name'] not in hide_toc])

    if uri and '.git' in uri:
        toc.append(f'Project Source <{uri}>')

    for file_info in files:
        clean_ups = []
        file_info = AttrDict(file_info)  # HACK: fixes issues when datatypes change
        if 'requirements' in file_info:
            requirements.extend(listish(file_info['requirements']))

        file_info.toc = toc
        file_info.version_info = version_info
        file_info.component = os.path.basename(src_dir)

        # merge and load values for file_info
        updated_values = {}
        if file_info.vals and not isinstance(file_info.vals, dict):
            if isinstance(file_info.vals, str):
                file_info['vals'] = [file_info.vals]
            for data_source in file_info.vals:
                with open(resolve_path(os.path.join(src_dir, data_source)),
                          'r') as values_file:
                    updated_values.update(get_scoped_values(
                        file_info.component,
                        file_info.name,
                        yaml.safe_load(values_file)['values']))

        # override values loaded here with anything from the env/--values-file
        updated_values.update(get_scoped_values(file_info.component,
                                                file_info.name,
                                                values))

        file_info['vals'] = updated_values
        # restructured file_info
        file_info = AttrDict(file_info)

        # generate inline tmp file
        if file_info.type == 'inline':
            inline_tmp = Generic.as_template(
                dst_dir, file_info,
                Templates.inline, template_env,
                multi_update(
                    {"content": file_info.get('content')},
                    file_info.vals
                ))
            file_info['src_paths'].extend(inline_tmp)
            clean_ups.extend(inline_tmp)

        # preparse file as a jinja template if has_jinja
        if file_info.has_jinja:
            tmp_files = RawFile.is_template(
                dst_dir, file_info, file_info.vals
            )
            file_info['src_paths'].append(tmp_files[0])
            clean_ups.append(tmp_files[0])

        if file_info.type == 'rst':
            doc_files.extend(RawFile.copy(dst_dir, file_info))

        elif file_info.type == 'md':
            doc_files.extend(RSTHelpers.convert_md(dst_dir,
                                                   file_info, template_env,
                                                   file_info.vals))

        elif file_info.type == 'inline' and file_info.has_jinja:
            try:
                os.rename(file_info.src_paths[-1], file_info.src_paths[0])
                clean_ups.remove(file_info.src_paths[0])
                doc_files.append(file_info.src_paths[0])
            except FileNotFoundError:
                raise Exception('Invalid paths for inline file during')

        elif file_info.type == 'text':

            doc_files.extend(RawFile.wrapped(
                src_dir, dst_dir, file_info,
                Templates.raw_file, template_env,
                multi_update(
                    {'name': file_info.name,
                     'highlight': file_info.highlight,
                     'description': file_info.description},
                    file_info.vals)
            ))

        elif file_info.type == 'openapi':
            doc_files.extend(
                RawFile.wrapped(
                    src_dir, dst_dir, file_info,
                    Templates.openapi, template_env,
                    multi_update(
                        {'description': file_info['description']},
                        file_info.vals)
                ))

        elif file_info.type == 'inline':
            doc_files.extend(
                Generic.as_template(
                    dst_dir, file_info,
                    Templates.inline, template_env,
                    multi_update(
                        {"content": file_info.get('content')},
                        file_info.vals
                    )
                ))

        elif file_info.type == 'autodoc':
            doc_files.extend(AutoDoc.as_template(
                dst_dir, file_info, template_env, file_info.vals
            ))

        elif file_info.type == 'raw':
            raw_files = RawFile.copy(dst_dir, file_info)
            doc_files.extend(raw_files)
            if file_info.add_toc:
                for file_dst in raw_files:
                    RSTHelpers.add_toc(
                        file_dst, file_info, template_env, file_info.vals
                    )

        clean_tempfiles(clean_ups)
    return doc_files, requirements


def listish(obj):
    """Returns a list of object or object if it is already a list"""

    if isinstance(obj, list):
        return obj
    else:
        return [obj]


def get_scoped_values(component, file_name, values):
    vals = {}
    for key in ('$GLOBAL',
                f'{component}$GLOBAL',
                f'{component}${file_name}',
                file_name):
        vals.update(values.get(key, {}))
    return vals


def multi_update(*args):

    obj = args[0]
    for dict_ in args[1:]:
        obj.update(dict_)
    return AttrDict(obj)


def dictish(obj, keys):
    """Returns a dictionary ensuring keys exist in object"""

    for key in keys:
        if key not in obj:
            obj[key] = None
    return obj


def clean_tempfiles(clean_ups):
    for path in clean_ups:
        try:
            os.remove(path)
        except FileNotFoundError:
            pass
