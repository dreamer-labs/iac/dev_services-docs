import datetime
import os


class Generic:

    @staticmethod
    def as_template(dst_dir, file_info, template, template_env, template_data):
        outfiles = []
        template = template_env.get_template(template)
        rst_file_name = f'{file_info.name}{os.path.extsep}rst'
        file_path_dst = os.path.join(dst_dir, rst_file_name)
        with open(file_path_dst, 'w') as outfile:
            outfile.write(template.render(
                name=file_info.name,
                page_title=file_info.get('title', file_info.name),
                file_type=file_info.type,
                component=file_info.component,
                toc=file_info.toc,
                date=datetime.datetime.now(),
                version_info=file_info.version_info,
                **template_data))
        outfiles.append(file_path_dst)
        return outfiles
