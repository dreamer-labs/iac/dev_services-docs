#!/bin/bash

sys_requirements=('pandoc' 'pip3' 'python3')

for req in ${sys_requirements[@]};
do
  if ! which $req >/dev/null;
  then
    echo "Missing system requirement ${req}, should be a binary in \$PATH";
    exit 1
  fi
done
