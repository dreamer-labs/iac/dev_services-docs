# PiperCI Sphinx Docs

## Bootstrapping a new project

The simplest option is to clone this repo and set config options in your make.conf to point to a DOCS_YML or RELEASE_YML. See examples provided for an example conf.py. The `source` directory should be used for a static conf.py and index.rst however a component may be defined to build the site_root "`site_root: yes`". This is recommended as it fully separates site content from site generation and CI/CD pipelines. Think, templates(jinja, css etc.) separate from content.

`/source` should be used for customizing template, css and other dynamic generation code as `source` is copied as the base for the `build` directory.

## Makefile

### Make configuration

`make.conf` is used to configure your docs site.

Important config values:

```
DESTDIR       = artifact  # Destination directory for the html site
BUILDDIR      = build     # Final build staging area for conf.py, css, and compiled rst files. Sphinx looks here to build your final site.
CACHEDIR      = cache     # The sphinx cache dir and pre-build staging cache directory for raw component git repositories.
SOURCEDIR     = source    # Your projects templates and source static content... you can use this for conf.py, and site root rst files if you choose, or use the dynamic option as well.
RELEASE       = altstadt  # The release name for your site, required by RELEASE_YML

## Build site from DOCS_YML or RELEASE_YML (mutually exclusive)
DOCS_YML      =           # Use a docs YAML file directly from this URI, supports http/s and local paths
RELEASE_YML   =           # Use a release YAML file + RELEASE to load the docs YAML file.
```

### Make Usage

`make component-requirements` installs requirements defined in the docs.yml required to build the html with sphinx
`make stage` downloads all the dependency sites defined in the `piperci-releases/release.yaml` or `docs.yml`
`make stage-cached` reuses previously staged sites defined in the `piperci-releases/release.yaml` or `docs.yml`
`make html` builds the html site and saves it to artifact for deployment
`make markdown` builds a markdown version of the site
`make pdf` build a single document pdf file (set screen target in make.conf)
`make requirements` installs requirements in the root requirements.txt and ensures base dependencies are installed prior to running stage
`make clean` the artifact (html site)
`make clean-cache` just the sphinx caches (not downloaded content in /build)
`make clean-build` the downloaded content in build (used to build the actual site)
`make clean-all` all the clean targets above

### Building

```
make requirements
make stage
make componenet-requirements
make html
```

## Building a single site from multiple sources

Docs site generation can be built from local(this) and remote(component) repositories. You may use a hybrid of static docs pages defined here and dynamically included files from other repositories. The end site is built using sphinx, but the site is defined in a docs.yml file located here or remotely.

### Docs.yml Syntax

### README.md auto-include

 To include a repositories README.md without getting fancy only add an entry for the repository and the README.md/rst will be auto added

 More specifically the [docs.yml](https://gitlab.com/dreamer-labs/piperci/piperci-releases/blob/master/altstadt/docs.yml) is used to configure your site.

### More granular files specifications

In addition to README auto-inclusion, additional files and options may be specified for each component.

```yaml
docs:
...
  - name: site_root
    site_root: true
    uri: "https://gitlab.com/dreamer-labs/piperci/piperci-docs.git"
    version: "master"
    files:
      - index.rst
      - piperci-functions.rst
      - conf.py
    requirements:
      - sphinx-theme

  - name: "project-info"
    uri: "https://gitlab.com/dreamer-labs/piperci/piperci-boilerplates.git"
    version: "master"
    files:
      - README.md
      - name: contributing
        path: CONTRIBUTING.md
      - name: LICENSE
        path: LICENSE
        type: text
        highlight: text
```

See the component and files sections for more detail

### Blend static with auto-gen docs

Both static and docs.yml defined content may be used to build your site.

Simply place any static rst files in `source` as source is first copied into place during each `make stage`

It is recommended however to keep your content separate from this config repo by defining a component included from an external repository.

### Components from a local directory

Component URI's may point to a local directory... for example, point to components in this repository each under their own sub-directory tree.

```
  - name: "my_component"
    uri: "components/my_component"
    version: "master"
    files:
      - README.md
      - name: contributing
        path: CONTRIBUTING.md
      - name: LICENSE
        path: LICENSE
        type: text
        highlight: text
```

### Docs.yml syntax

#### Component Directives

```yaml
name: :string: The name of the target project directory for example 'python-piperci' which would create the component at /python-piperci
uri: :string: The git clone URI or local file path to a component(sub-directory). It must be a git URI with access by the build user.
version: :string: The tag, branch or hash to checkout
files: :list: An optional list of "files" definitions. Optional if you want more then the README.md/rst auto-include behavior. Files may be a list of strings or dictionaries with further options. See: files options.
site_root: :boolean: A flag to indicate that the content here should be saved to the site root. This is used to indicate that the named component is copied to the site root. Example: /index.rst rather then /my_compontent/index.rst etc.
requirements: :list: A list of python requirements for this component
```

Supported "file" level directives are defined below. Options are separated by type starting with global shared file directives.

#### Shared File Directives

```yaml
name:
  - ":string: The name of the file entry as it will live in the docs"
  - ":required:"
path:
  - ":string: The relative path to the file in the source project."
  - ":default: name"
type:
  - ":string: A documentation file type."
  - ":choices: ['md', 'rst', 'autodoc', 'text', 'openapi', 'inline', 'raw']"
  - ":default: [md -> rst] in order."
add_toc:
  - ":boolean: If true will attempt to add/append a sub-section table of contents to the end of this file."
  - ":default: false"
add_indexes:
  - ":boolean: If true will attempt to add/append a sub-section table of indexes to the end of this file."
  - ":default: false"
hide_toc:
  - ":boolean: If true will hide this file from all toc blocks, useful if this file is a raw file like a conf.py or image file."
  - ":default: false"
has_jinja:
  - ":boolean: If true will parse this file first as a jinja template and then render through the rest of the defined converters.
  - :default: false"
values:
  - ":list|string: Load values for has_jinja above from this file or files. See notes on scoping values. also a global values file may be specified at run time on the CLI."
  - "specify paths as system absolute or relative from the base of your component."
```

#### Values Directive

Values YAML files expose values in the following order, the last value taking precedence.

- $GLOBAL
- {component_name}$GLOBAL
- {component_name}${file_name}

"{component_name}" will be matched to the name used for your defined component's name: directive
"{file_name}" will be matched to the name of the file's name: directive
GLOBAL is a key that will be exposed to anything under that scope... so $GLOBAL applies to anything, my-component$GLOBAL to all files in my-component etc.

```yaml
---
values:
  $GLOBAL:
    value1: "A hash/dict with values that will be exposed to has_jinja files"
    some_variable_name: "any valid hash key"
  some-component$GLOBAL:
    value1: "overrides $GLOBAL.value1 in all files in some-component"
    value2: "will be exposed to any file in some-component as simply {{ value2 }}"
  some-component$a-specific-file:
    value2: "overrides some-component$GLOBAL in a-specific-file exposed as {{ value2 }}"
    y: "{{ y }} specific values in a-specific-file"
    z: "another value in a-specific-file exposed as {{ z }}"
```

#### Directives for File Types

##### MD and RST File Directives

MD and RST types are treated almost the same, except that MD files are first converted to sphinx RST files while RST is treated almost as a raw file and simply copied into place prior to sphinx processing.

There are no extra special directives.

```yaml
type: ":string: (md|rst)"
```

##### Autodoc File Directives

```yaml
type: :string: "autodoc"
title: :string: A title to be used for this autogenerated page
description: :string: A freeform text blob that will be displayed below the "title".
programs:
  - ":string: A program definition (text/code block with program usage/help etc.)"
  - ":list: A list of program definiation text blocks"
imports:
  - ":string: A string with one import directive... for this entire page"
  - ":list: A list of import directives showing users how to import these tools"
  - ":example: from x.y import z"
modules:
  - ":list: A list of module definitions"
  - ":required:"
  - ":example: See below of"
  - heading: :string: Text for this modules "heading" h2
    module:
      - ":string: The python module autodoc should import... x.y"
      - ":example: piperci.sri"
    imports:
      - ":string: A string with one import directive... specific for this module"
      - ":list: A list of import directives showing users how to import these tools"
      - ":example: from x.y import z"
requirements:
  - ":list: A list of pip -r requirements.txt entries"
```

##### Inline File Directives

This will be an RST file but contents may be defined in the contents field.

```yaml
contents: ":string: File contents"
type: ":string: inline"
```

##### Raw file Directives

Raw files are copied into place, with no file wrapper page. For a wrapped file with a highlighted text options etc. use the type: text instead.

```yaml
type: ":string: raw"
```

##### Text File Directives

Type text will be treated as a wrapped file, copied into place with an embedded text box as the primary documented page.

The description directive adds a text description above the embedded highlighted text content.

```yaml
description: ":string: Description"
highlight: ":string: pygments language lexer http://pygments.org/docs/lexers/ shortnames"
type: ":string: text"
```

##### Openapi Directives

Openapi directive is similar to text except it generates an openapi autodoc page and an text highlighted page for the openapispec file.

```yaml
description: ":string: Description"
type: ":string: openapi"
```
