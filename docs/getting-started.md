# Getting Started

This documentation will guide you through the installation and configuration of a full suite of applications running on a ceph-backed kubernetes cluster running on Proxmox or Openstack.

## Pre-Airgapped Prep

### Prerequisites

This guide assumes that you have a `deploy node` from which to run your deployment inside the air-gapped network in addition to an Internet-connected `staging node`. These can be any workstations or VMs that meet the following requirements:

- docker
- docker-compose
- pigz
- at least 80GB of available disk space

We provide a base image of ubuntu with docker pre-installed in Proxmox. See [environment.html](environment.html) for details.

In addition, be sure to have the following available before you begin:

- A temporary CA certificate to use for pre-airgap configuration
- A root-signed intermediate cert and private key for the target environment

### Clone the repository

From your Internet-connected `staging node`:

```bash
git clone https://gitlab.com/dreamer-labs/iac/dev_services.git
cd dev_services/
export SVC_HOME=${PWD}
# SVC_HOME is a variable we will use a lot. Save it in your .bashrc
grep -q 'SVC_HOME' ~/.bashrc || echo "export SVC_HOME=${PWD}" >> ${HOME}/.bashrc
touch ~/.vault-file
```

### Configure your environment variables

> This is probably the most important step! After sourcing `env.sh` be **certain** that the variables make sense for your environment. You may need to change `RANCHER_FQDN`. `ROOT_CA` and `KUBECONFIG` don't matter yet. We will set those later as we create them.

`env.sh` will set a number of environment variables on your system and make them persistent. It will output these variables to stdout for you to review. You can override any of the default/interpretted values by exporting that variable in your current shell, and re-sourcing `env.sh`

```bash
cd ${SVC_HOME}
. env.sh
```

> If any variables are wrong, `export` that variable and re-source `env.sh`. From now on that variable will be set, both inside and out of the nawt container, even after a new login/reboot. The values are stored in `${HOME}/.paas/env`

### Generating temporary certificates for our registry

Our registry requires a certificate to run. As we don't yet have access to our vault (it's in the container) we will generate a quick temporary certificate:

```
cd $SVC_HOME/deploy-tools

docker run -v $SVC_HOME/deploy-tools/certs:/certs \
  -e CA_SUBJECT=registry-ca \
  -e SSL_SUBJECT=registry.devservices.int \
  -e SSL_DNS=registry.devservices.int \
  -e SSL_IP=${REGISTRY_HOST_IP} \
  -e SSL_KEY=tls.key \
  -e SSL_CERT=tls.crt \
  -e CA_CERT=cacerts.pem \
  -e SSL_SIZE=4096 \
  -e SSL_EXPIRE=120 \
   superseb/omgwtfssl
```

### Building the nawt container

Still on your Internet-connected `staging node`:

```bash
cd ${SVC_HOME}/deploy-tools

docker-compose build
docker-compose up -d
```

### Download Images

All of the included applications contain a file called `images.txt` in their respective directories. We will use a script to combine those into one large file and then download the images required for air-gap:

From your `staging node`, run:

```bash
cd ${SVC_HOME}/rke
./combine-images.sh
./rancher-save-images.sh -l combined-images.txt -i combined-images.tar.gz
```

combined-images.tar.gz contains all the necessary images for an air-gapped deployment. Leave this file in the current directory. In the next step we will transfer the file into the air-gap.

### Crossing the gap

Package up all the files required inside the gap, including a copy of the dev_services repository and combined-images.tar.gz.

From your `staging node`, run:

```bash
cd ${SVC_HOME}/..
tar -czvf dev_services.tar.gz dev_services
```

## In the Airgapped environment

### Prepare the deploy node

You should have a machine/VM dedicated to act as your `deploy node` inside the air-gap just as you had a staging node outside. It has the same software requirements of docker, docker-compose, and pigz.

Copy the dev_services.tar.gz created outside the air-gap to this machine and then run:

```bash
tar -xzf dev_services.tar.gz
cd dev_services
export SVC_HOME=${PWD}
# Add SVC_HOME to your .bashrc for convenience
grep -q 'SVC_HOME' ~/.bashrc || echo "export SVC_HOME=${PWD}" >> ${HOME}/.bashrc
```

### The nawt container

The deploy-tools nawt docker image is intended to provide a complete set of tools which facilitate an air-gapped installation. It is also a consistent environment from which all deployment tooling may be tested.

Tools provided in the nawt image:

- ansible
- ceph-common (ceph)
- jq
- vim
- qemu-utils (qemu-img etc.)
- hashicorp vault
- docker registry
- kubectl
- rke
- helm
- rancher cli
- docker client tools
- net utils

Two additional containers are supplied via docker-compose:

- hashicorp vault server exposed on port 8200
  Used for storage of secrets required during bootstrapping.
- docker registry exposed on port 5000
  Used to store the required container images used to install rancher/kubernetes.

### Building the nawt container

```bash
cd ${SVC_HOME}/deploy-tools

docker-compose build
docker-compose up -d
. ../env.sh
```

> NOTE: Make sure that you read all the values output by `env.sh` and update any variables that are incorrectly identified (i.e. you are using a different FQDN, or the wrong public interface is selected by the script). Make sure to use `export` when saving the variables, and RE-SOURCE `env.sh` after making any required changes. Your variables will now be set in `~/.paas_env` and will be re-loaded from your `~/.bashrc` every time you login.

### Running the nawt container

The nawt container can be launched at any time, and can be executed using a function defined in env.sh:

```bash
nawt
```

### SSH keys

Generate an SSH key from your `deploy node`:

> If you already have an SSH you wish to use, just make sure it is in-place on your staging box with appropriate permissions, and have the public key available to place in the cloud-init section of proxmox.

```bash
ssh-keygen -t rsa -b 4096 -q -N "" -C "dreamerlabs@us.globalinfotek.com"
cat ~/.ssh/id_rsa.pub
```

> Make a note of this ssh key. You will use this public key when configuring cloud-init for your proxmox virtual machines.

### Build your environment in Proxmox

Currently we do not have automation for building the VMs in Proxmox. See [environment.html](environment.html) for detailed instructions on building VMs in Proxmox.
