# MetalLB

## Configuration
Edit values.yaml and modify the IP address range to fit your environment

## Installation
```bash
nawt

cd /dev_services/metallb

# On first install only
kubectl create namespace metallb-system
kubectl create secret generic -n metallb-system metallb-deployment-memberlist --from-literal=secretkey="$(openssl rand -base64 128)"

# Deploy with helm
helm upgrade -i metallb-deployment metallb --namespace metallb-system -f values.yaml
```