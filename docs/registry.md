# Deploying the registry

This section will configure a local containerized registry hosted via docker-compose and populated with the contents of `combined-images.tar.gz` that we created in the "Getting Started" section.

This registry will be used to deploy all the various components of our infrastructure.

## Generate a new certificate

Create a new TLS certificate using vault:
From **inside** the nawt container, run:

> REGISTRY_HOST_IP is the IP address of your local machine, and the address where you expect airgapped kubernetes nodes to be able to reach you to retrieve their packages.

```
nawt # Enter our nawt container
mkdir -p /dev_services/_certs/registry

vault write ${ENVIRONMENT}/pki_int/issue/devservices_int \
  common_name=${REGISTRY_HOST_FQDN} \
  ttl=${TLS_TTL} \
  ip_sans="127.0.0.1,${REGISTRY_HOST_IP}" \
  --format=json > /dev_services/_certs/registry/certs-combined.json

jq -r '.data.certificate' /dev_services/_certs/registry/certs-combined.json > /dev_services/_certs/registry/tls-tmp.crt
jq -r '.data.issuing_ca' /dev_services/_certs/registry/certs-combined.json > /dev_services/_certs/registry/cacerts.pem
jq -r '.data.private_key' /dev_services/_certs/registry/certs-combined.json > /dev_services/_certs/registry/tls.key

# Build tls-intermediate chain
cat /dev_services/_certs/registry/tls-tmp.crt /dev_services/_certs/registry/cacerts.pem > /dev_services/_certs/registry/tls.crt
```

The commands above generate four files:

- /dev_services/_certs/registry/tls-tmp.crt       # certificate chain
- /dev_services/_certs/registry/tls.crt           # certificate
- /dev_services/_certs/registry/cacerts.pem       # issuing_ca
- /dev_services/_certs/registry/tls.key           # private_key

The registry will read the certificates from `/dev_services/deploy-tools/certs/`.

You should verify that your new deploy registry is in working order.

From **outside** the nawt container, load the CA certificate into your deployment workstation and restart docker:

> Docker must be restarted in order to pick up the new trusted root CA

## Trust the registry

```
sudo cp ${ROOT_CA} /usr/local/share/ca-certificates/registry-ca.crt
sudo update-ca-certificates --fresh
sudo systemctl restart docker
cd $SVC_HOME/deploy-tools
docker-compose up -d
```

## Test your registry

```
docker pull vault
docker image tag vault $REGISTRY_HOST_FQDN/vault
docker push $REGISTRY_HOST_FQDN/vault
docker pull $REGISTRY_HOST_FQDN/vault
```

## Populate registry (air-gapped)

This command will upload all of the images listed in `combined-images.txt`  and store them in our private registry:

```
cd $SVC_HOME/rke
./rancher-load-images.sh -l combined-images.txt -i combined-images.tar.gz -r $REGISTRY_HOST_FQDN
```