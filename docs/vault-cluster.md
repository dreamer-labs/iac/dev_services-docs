# Hashicorp Vault

This guide is intended to help you install hashicorp vault as a highly-available cluster backed by cephfs using vault's internal `raft` storage.

First we need to generate some TLS keys. You can do this anyway you like, but we use a local vault for this. The vault we use can be found in the https://gitlab.com/dreamer-labs/iac/dev_services repo.

See (PKI.html)[PKI.html] for details on how to setup your own local vault server to issue certificates.

## Generate certificates

```
vault write dev/pki/issue/dldev-dot-xyz common_name=vault.dev.dldev.xyz \
  alt_names="*.vault-internal.vault-dev.svc.cluster.local,*.vault-internal" \
  ip_sans="127.0.0.1" ttl=1300h > vault-dev.certs
```

This will create a file called `vault-dev.certs` which will contain the `certificate`, `key`, and `issuing ca`.

Use the fields inside `vault-dev.certs` to create the files `tls.crt`, `tls.key`, and ca.pem`, respectively.

Connect to the kubernetes cluster by sourcing a kubeconfig file and create your namespace:
```
kubectl create ns vault-dev
```

Upload our TLS certificates as a secret:
```
kubectl create secret generic vault-tls-secret -n vault-dev --from-file=tls.key --from-file=tls.crt --from-file=ca.pem
```

These secrets, and this namespace, will persist even if you uninstall/reinstall vault, so if you wish to overwrite them you must specifically delete them with `kubectl delete secret vault-tls-secret -n vault-dev`

We are now ready to rollout our deployment of vault:
```
helm repo add hashicorp https://helm.releases.hashicorp.com
helm repo update
helm upgrade -i -f values.yaml -n vault-dev vault hashicorp/vault
```

> `helm upgrade -i` will install if the deployment doesn't exist, so we just always use this.

Run the following:
```
kubectl get pods -n vault-dev
```

> You should see after a few minutes that the pods are RUNNING but are *NOT READY*! This is because the vault has not yet been initialized or unsealed.

```
# kubectl get pods -n vault-dev
NAME                                   READY   STATUS    RESTARTS   AGE
vault-0                                0/1     Running   0          21m
vault-1                                0/1     Running   0          39m
vault-2                                0/1     Running   0          39m
vault-agent-injector-cdd7876d5-svkdx   1/1     Running   0          24m
```

To initialize the vault, execute the following:
```
kubectl exec vault-0 -n vault-dev -- vault operator init -key-shares=1 -key-threshold=1 -format=json > cluster-keys.json
```
> NOTE: In production you should always implement additonal `key-shares` and `key-threshold`. This is considered very insecure to use only one.

This will generate a file called cluster-keys.json in your local directory that contains the information you need to unseal and login to the vault.

Example cluster-keys.json:
```
{
  "unseal_keys_b64": [
    "SCcBtkISqGbyinB2KHE5zeUm4JwDUZuQyYFfweafjNo="
  ],
  "unseal_keys_hex": [
    "482701b64212a866f28a7076287139cde526e09c03519b90c9815fc1e69f8cda"
  ],
  "unseal_shares": 1,
  "unseal_threshold": 1,
  "recovery_keys_b64": [],
  "recovery_keys_hex": [],
  "recovery_keys_shares": 5,
  "recovery_keys_threshold": 3,
  "root_token": "s.XZSVsrGBreL3MAGN2sgolse7"
}
```

The information we need is contained in the `unseal_keys_b64` and `root_token` fields.

Connect to the `vault-0` pod:
```
kubectl exec -ti vault-0 -n vault-dev /bin/sh
```

Once connected to `vault-0` execute the following commands:
```
# Using the unseal_keys_b64 found in cluster-keys.json
vault operator unseal SCcBtkISqGbyinB2KHE5zeUm4JwDUZuQyYFfweafjNo=

# Using the root_token found in cluster-keys.json
vault login s.XZSVsrGBreL3MAGN2sgolse7
```

You can now confirm that vault-0 is part of a raft cluster:
```
vault operator raft list-peers
```

You should see a single peer represenging vault-0.

## Add additional peers

Connect to each additional peer pod like so:
```
kubectl exec -ti vault-X -n vault-dev /bin/sh
```

Once on the pod, execute:
```
export CA_CERT=$(cat /vault/userconfig/vault-tls-secret/ca.pem)
vault operator raft join -leader-ca-cert="$CA_CERT" https://vault-0.vault-internal:8200
```

Hopefully you have seen a message saying that you have joined. Unseal and login to your vault on `vault-X` as above:
```
# Using the unseal_keys_b64 found in cluster-keys.json
vault operator unseal SCcBtkISqGbyinB2KHE5zeUm4JwDUZuQyYFfweafjNo=

# Using the root_token found in cluster-keys.json
vault login s.XZSVsrGBreL3MAGN2sgolse7
```

Repeat for all of your vault pods!

You now have a highly available vault.
