# Docker Harbor

## Installation

```bash
export HARBOR_NS=harbor-{{ environment }}
kubectl create namespace ${HARBOR_NS} # If namespace doesn't exist

## Create TLS certs
##
mkdir -p /dev_services/_certs/harbor
vault write ${ENVIRONMENT}/pki_int/issue/devservices_int \
  common_name={{ hostname }}.{{ domain }} \
  alt_names=
  {%- for d in alt_domains -%}
         {{ hostname }}.{{ d }},
  {%- endfor -%}{{ hostname }}.devservices.int,*.docker.io \
  ttl=${TLS_TTL} \
  --format=json > /dev_services/_certs/harbor/certs-combined.json

jq -r '.data.certificate' /dev_services/_certs/harbor/certs-combined.json > /dev_services/_certs/harbor/tls.crt
jq -r '.data.issuing_ca' /dev_services/_certs/harbor/certs-combined.json > /dev_services/_certs/harbor/ca.crt
jq -r '.data.private_key' /dev_services/_certs/harbor/certs-combined.json > /dev_services/_certs/harbor/tls.key

## Create TLS secret
kubectl create secret generic harbor-tls-secret \
  --from-file=ca.crt=/dev_services/_certs/harbor/ca.crt \
  --from-file=tls.crt=/dev_services/_certs/harbor/tls.crt \
  --from-file=tls.key=/dev_services/_certs/harbor/tls.key \
  -n ${HARBOR_NS}

# Deploy Harbor
vault kv put ${ENVIRONMENT}/secrets/harbor \
  secretKey="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)" \
  admin="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)"

helm upgrade -i harbor harbor -n ${HARBOR_NS} \
  --set externalURL=https://{{ hostname }}.{{ domain }} \
  --set expose.loadBalancer.IP={{ endpoint }} \
  --set secretKey=$(vault kv get --format json ${ENVIRONMENT}/secrets/harbor | jq -r '.data.data["secretKey"]') \
  --set harborAdminPassword=$(vault kv get --format json ${ENVIRONMENT}/secrets/harbor | jq -r '.data.data["admin"]') \
  -f values.yaml

# Store our currently deployed helm values file in the vault:
vault kv patch ${ENVIRONMENT}/secrets/harbor values.yaml=@<(helm get values -n ${HARBOR_NS} harbor)
```

## Administration

The default username and password can be found in the values.yaml file. THESE SHOULD NEVER BE USED.

## Notes

This helm chart has been modified from the original Harbor Helm chart hosted online. When upgrading this chart you may have to reimplement some fixes.

Notably, the current version of Harbor Helm upstream does not set permissions correctly in the database pod. This helm chart has a modified init container to correctly set permissions.

## Add OAuth

Inside of Gitlab:
1. Goto Gitlab deployment: https://git.{{ domain }} -> Admin -> Applications -> Add New Application

```
Name: harbor OR harbor-{{ environment }}
Callback URL: https://<Harbor deployment URL>/c/oidc/callback
Trusted: Y
Confidential: Y
Scopes: api,read_user,read_api,openid,profile,email
```

Inside of Harbor
Adminstration->Configuration->Authentication
Change Auth mode to OIDC

```
Provider: Gitlab
OIDC Endpoint: https://git.<depoyment url>
OIDC Client ID: <app id>
OIDC Client Secret: <secret>
Group Name: blank
OIDC Scope: api,openid,read_user,read_api,profile,email
```
