# Prepare the IaaS

Before we begin, we need a platform to work from.

Documentation for building a basic 3-node cluster on Proxmox can be found here:
https://gitlab.com/dreamer-labs/iac/dev_services_paas/-/blob/master/docs/proxmox-install.md

## Importing a virtual machine image

Once the IaaS has been created, you will need to import some baseline images to use for your VMs.

In this example, we will import a basic vanilla ubuntu cloud vm with cloud-init and assign it an id of `9000`:

> Run these commands as the root user on one of the proxmox nodes!

```bash
apt-get install cloud-init

# download the image
wget https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img

# create a new VM
qm create 9000 --memory 2048 --net0 virtio,bridge=vmbr0

# import the downloaded disk to vms storage
qm importdisk 9000 bionic-server-cloudimg-amd64.img vms

# finally attach the new disk to the VM as scsi drive
qm set 9000 --scsihw virtio-scsi-pci --scsi0 vms:vm-9000-disk-0

qm set 9000 --ide2 vms:cloudinit

qm set 9000 --boot c --bootdisk scsi0

qm set 9000 --serial0 socket --vga serial0
```

You can further customize this image in the proxmox GUI and then save it as a template when you're done. You can convert it to template either in the GUI, or with the `qm template 9000` command.

## Configuring a base image

The ubuntu image above is a great starting point for an internet-connected installation, but you may need to have an image with docker pre-installed for an air-gapped deployment.

### Use a pre-existing base image

We provide an ubuntu 18.04 base image with docker 19.03.9 pre-installed:

```bash
wget https://storage.uk.cloud.ovh.net/v1/AUTH_f89509e8bded4213b0b0c14e013b9b66/paas/ubuntu18.04-docker19.03.9.qcow2
```

Simply use this image in the `qm importdisk` step above. I like to use instance id `9004` for this instance, as that is what is outlined in the rest of the documentation.

### Building a custom base image

You may need to build a custom base image. The process is easy:

1. Build a VM in openstack
2. Install any software that you want onto the base image, such as docker: (https://docs.docker.com/engine/install/ubuntu/)
3. Take a snapshot of the running instance.
4. Download the snapshot image using the openstack cli:
   glance image-download –file <Name-of-Cloud-Image> –progress  <Image_ID>
5. Optionally, to trim down the size of the image, run `qemu-img convert -p -f raw <Name-of-Cloud-Image>.qcow2 -O qcow2 <Name-of-Cloud-Image>-reduced.qcow2
6. Upload the file to glance for permanent public storage, or copy it onto your proxmox node.

## Get started!

You are now ready to move onto the getting-started section and start deploying your PaaS!