# Creating the kubernetes clusters

In this section we will create two clusters. The first is a multi-node cluster used to host our applications, and the second is single-node cluster used to manage the primary cluster with rancher.

## Configuring the cluster(s) with rke

You will run the following command **twice** from **within** the nawt container. Once for the multi-node kubernetes cluster, and once for the single-node rancher cluster:

Edit the file `${SVC_HOME}/rke/configure.sh.cfg` and create your deployment node configurations

The configuration section must match the value you use for the DEPLOYMENT_NAME for example:

  `DEPLOYMENT_NAME="cluster_RANCHER"` would load the config section cluster_RANCHER_kube_nodes=$(your HEREDOC)

The file `configure.sh.cfg` should look exactly like this:

```bash
#!/usr/bin/env bash

# deployment defaults
#REGISTRY_HOST_PORT=443
REGISTRY_HOST_FQDN=${REGISTRY_HOST_FQDN:-registry.devservices.int}

# Define deployment node answers will be accessed like $DEPLOYMENT_NAME_kube_nodes
cluster_{{ environment }}_kube_nodes=$(
cat <<EOF
{% for i in kube_public_ips %}
      {%- set host = kube_hostnames[loop.index-1] %}
      {%- set pub_ip = kube_public_ips[loop.index-1] %}
      {%- set priv_ip = kube_private_ips[loop.index-1] %}
           {{- host }} {{ pub_ip }} {{ priv_ip }}
{% endfor -%}
EOF
)

cluster_RANCHER_kube_nodes=$(
cat <<EOF
rancher {{ rancher_public_ip }} {{ rancher_private_ip }}
EOF
)
```

Once the `${SVC_HOME}/rke/configure.sh.cfg` is setup continue preparing the deployment from **inside** the nawt container:

```bash
nawt
cd /dev_services/rke
./configure.sh cluster_${ENVIRONMENT}
./configure.sh cluster_RANCHER
```

> You will be prompted to answer some basic information about our cluster. We have reserved ips `*.101, *.102, and *.103` for our kubectl nodes and `*.105` for our rancher node. We run this script twice. The first time we will specify three nodes to act as our kubernetes cluster, the second time, we will specify one or more additional nodes to be our rancher cluster. Be sure to set the following IPs accordingly for the nodes you provisioned in [environment.html](environment.html)

Example output:

```text
## Deployment Name ##

Enter DEPLOYMENT_NAME
      (ex: cluster_{{ environment }}) []: dev

## Define Kubernetes nodes ##

Enter answer
      (ex: Found node definitions in dev_kube_nodes would you like to use those? [y]/n: ) []:
Nodes defined for this environment: kube-1 kube-2 kube-3
REGISTRY_CONNECT is set to registry.devservices.int, if you expect a specific port define with REGISTRY_HOST_PORT

## Create an /etc/hosts alias for registry.devservices.int ##

Enter REGISTRY_HOST_IP
      (ex: Enter the IP of a docker registry) [enter, to skip creating host entry]: {{ registry_ip }}

## Rancher setup info ##

(if the deployment is to be a rancher cluster just enter the public rancher gateway anyhow)
Enter RANCHER_IP
      (ex: IP of rancher cluster) [enter skips]: 10.1.1.105
Enter RANCHER_FQDN
      (ex: FQDN of the rancher cluster) [enter skips]: rancher.devservices.int

## Generating Deployment ##

kube-1 kube-2 kube-3
registry.devservices.int
rancher.devservices.int
```

The command above will output the following files:

- cluster_${ENVIRONMENT}.yml
- cluster_${ENVIRONMENT}-coredns.yml
- cluster_${ENVIRONMENT}_inventory.ini
- kube_config_cluster_${ENVIRONMENT}.yml
- cluster_${ENVIRONMENT}-inventory.ini

The `configure.sh` command above will generate a file called `cluster_${ENVIRONMENT}.yml` in the current working directory. We can use this file to deploy and modify our clusters prior to deployment.

## Build the cluster(s)

From **inside** the nawt container:

```bash
nawt
cd /dev_services/rke
rke up --config cluster_${ENVIRONMENT}.yml
rke up --config cluster_RANCHER.yml
```

> Save a copy of `cluster_{{ environment }}.yml` and `cluster_RANCHER.yml` someplace safe, such as a vault. It can be used for upgrading/modifying/destroying the cluster at later times.

## Setup coreDNS

CoreDNS provides necessary name resolution inside the cluster.

`/dev_services/rke/configure.sh` should have generated coreDNS resource files. Apply those to both clusters to properly configure DNS resolution and prevent DNS loops.

```bash
nawt
cd /dev_services/rke

export KUBECONFIG=/dev_services/rke/kube_config_cluster_{{ environment }}.yml
kubectl apply -n kube-system -f cluster_{{ environment}}-coredns.yml

export KUBECONFIG=/dev_services/rke/kube_config_cluster_RANCHER.yml
kubectl apply -n kube-system -f cluster_RANCHER-coredns.yml
```