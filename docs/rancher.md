# Rancher

## Deploying Rancher

### Creating the namespace
Connect to your new rancher cluster with `kubectl` by exporting the `$KUBECONFIG` variable and install rancher:

```bash
nawt
export KUBECONFIG=/dev_services/rke/kube_config_cluster_RANCHER.yml
kubectl create namespace cattle-system
```

### Create an admin password

Let's create an admin password and store it into vault. We'll use this the first time we login to the web interface.

```bash
vault kv put ${ENVIRONMENT}/secrets/rancher admin=$(openssl rand -base64 16)
```

### Adding the rancher TLS certificates

Create a new TLS certificates using vault:

```bash
nawt # Enter our nawt container
mkdir -p /dev_services/_certs/rancher

vault write ${ENVIRONMENT}/pki_int/issue/devservices_int \
  common_name={{ hostname }}.{{ domain }} \
  alt_names=
  {%- for d in alt_domains -%}
         {{ hostname }}.{{ d }},
  {%- endfor -%}{{ hostname }}.devservices.int \
  ttl=${TLS_TTL} \
  --format=json > /dev_services/_certs/rancher/certs-combined.json

jq -r '.data.certificate' /dev_services/_certs/rancher/certs-combined.json > /dev_services/_certs/rancher/tls.crt
jq -r '.data.issuing_ca' /dev_services/_certs/rancher/certs-combined.json > /dev_services/_certs/rancher/cacerts.pem
jq -r '.data.private_key' /dev_services/_certs/rancher/certs-combined.json > /dev_services/_certs/rancher/tls.key
cat /dev_services/_certs/rancher/cacerts.pem >> /dev_services/_certs/rancher/tls.crt
```

This command generates three files of output:

- /dev_services/_certs/rancher/tls.crt       # certificate chain
- /dev_services/_certs/rancher/cacerts.pem   # issuing_ca
- /dev_services/_certs/rancher/tls.key       # private_key

Create rancher secrets

```bash
nawt
export KUBECONFIG=/dev_services/rke/kube_config_cluster_RANCHER.yml

kubectl -n cattle-system create secret tls tls-rancher-ingress \
  --cert=/dev_services/_certs/rancher/tls.crt \
  --key=/dev_services/_certs/rancher/tls.key

kubectl -n cattle-system create secret generic tls-ca \
  --from-file=/dev_services/_certs/rancher/cacerts.pem
```

### Installing rancher

```bash
nawt

cd /dev_services/rancher

export KUBECONFIG=/dev_services/rke/kube_config_cluster_RANCHER.yml
export NETWORK_NAME={{ name }}

helm upgrade -i rancher rancher -f values.yaml \
       --namespace cattle-system \
       --version v2.4.3 \
       --set hostname=${RANCHER_FQDN} \
       --set rancherImage=${REGISTRY_HOST_FQDN}/rancher/rancher \
       --set ingress.tls.source=secret \
       --set privateCA=true \
       --set useBundledSystemChart=true \
       --set rancherImageTag=v2.4.3 \
       --set busyboxImage=${REGISTRY_HOST_FQDN}/rancher/busybox \
       --set ingress.hosts[0]=rancher.{{ domain }} \
       --set ingress.hosts[1]=rancher.staging.{{ domain }} \
       --set ingress.hosts[2]=rancher.prod.{{ domain }} \
       --set ingress.hosts[3]=rancher.devservices.int \
       --set systemDefaultRegistry=${REGISTRY_HOST_FQDN}

# Store our currently deployed helm values file in the vault:
vault kv patch ${ENVIRONMENT}/secrets/rancher values.yaml=@<(helm get values -n cattle-system rancher)
```

## Using Rancher for the first time

### Add the trusted CA to your browser

This is different depending on your browser, in Firefox go to `Options`, `Security & Privacy`, scroll all the way to the bottom of the page where it says `View certificates` and then click `Import` to import the cacerts.pem file into Firefox so that your browser will trust rancher (and all the apps deployed in rancher).

> Cert file should be available at `${ROOT_CA}`

### Connecting to Rancher

Login to rancher at `https://rancher.devservices.int`. If it gives you a 503 error, just be patient! It sometimes takes a few minutes to start up.

> NOTE: If you are proxying through a bastion for this connection, you will need to update `/etc/hosts` on the bastion/proxy server! for rancher.devservices.int

### Importing an external cluster

From **inside** the nawt container:

- Login to the rancher web interface and use the menu at the top-left to navigate to the global space.
- Click the "Add Cluster" button and select "Import an existing cluster"
- Give the cluster a name and click "Create"

Rancher will give you a kubectl command which you will run against the cluster you wish for rancher to manage and then run the kubectl commmand.

> Remember to configure your kubectl for the `dev` cluster before running the commands from rancher

```bash
nawt
export KUBECONFIG=/dev_services/rke/kube_config_cluster_{{ environment }}.yml
```

Rancher will connect to our dev cluster and can now be used to manage it.

### Validation

Verify that there are no pods in CrashLoop within the cattle-system namespace.

```bash
nawt
export KUBECONFIG=/dev_services/rke/kube_config_cluster_{{ environment }}.yml
kubectl -n cattle-system get pods
```

### Enable monitoring

Rancher has built-in prometheus monitoring.

- Login to the rancher web interface and use the menu at the top left to navigate to the dev cluster.
- Select the "Tools" dropdown menu and select "Monitoring"

> We have included the helm chart for rancher-v2.4.3 in the `/dev_services/rancher/rancher` of this repository.


## Rotating Rancher TLS certificates and/or modifying DNS

```bash
nawt
export KUBECONFIG=/dev_services/rke/kube_config_cluster_RANCHER.yml

kubectl -n cattle-system create secret generic tls-ca \
  --from-file=/dev_services/_certs/rancher/cacerts.pem

kubectl get clusters

# Run the following command for each cluster returned above
kubectl patch cluster <CLUSTER_ID> -p '{"status":{"agentImage":"dummy"}}' --type merge
```

**Detailed notes for above**

Steps to change the URL of Rancher installation and switch from a self-signed certificate to a certificate signed by recognized CA.

1. Change the Rancher `server-url` setting to the new URL:
  - Navigate to `https://<old_rancher_hostname>/g/settings/advanced`
  - Edit `server-url` to `https://<new_rancher_hostname>`
2. Clear the private CA certificate for the old certificate
  - Navigate to `https://<old_rancher_hostname>/g/settings/advanced`
  - Next to `cacerts` click context menu -> View in API
  - Click Edit
  - Clear the content of the `value` field
  - Click Show Request then Send Request
3. Trigger a re-deployment of the cluster-agent and node-agent for each cluster:
  - Configure `kubectl` to point at the RKE cluster where Rancher server is running
  - Note down the IDs of the managed clusters: `$ kubectl get clusters`
  - For each cluster (including `local`) run:
  ```
  $ kubectl patch cluster <CLUSTER_ID> -p '{"status":{"agentImage":"dummy"}}' --type merge
  ```
4. Note that this will disconnect all clusters from Rancher until the installation is upgraded with the new hostname / ingress configuration.
5. Update the certificate for Rancher from private to public signed one:
  - Delete the old certificate/key pair secret, ie. `$ kubectl -n cattle-system delete secret tls-rancher-ingress`
  - Add the new certificate/key pair secret (https://rancher.com/docs/rancher/v2.x/en/installation/ha/helm-rancher/tls-secrets/).
  - Remove the private CA certificate, `$ kubectl -n cattle-system delete secret tls-ca`
6. Upgrade Rancher installation using the `helm upgrade` command following the steps here: https://rancher.com/docs/rancher/v2.x/en/upgrades/upgrades/ha-server-upgrade-helm/#upgrade-rancher
  - Specify the currently installed Rancher version to prevent software upgrade
  - Pass all the values (`--set`) that were originally specified during installation
  - Pass the new Rancher hostname in the `hostname` variable
  - Ensure you specify `--set privateCA=false` to clear out the old private CA certificate
