# Ceph (Hyperconverged)

> These steps are only required when deploying to OpenStack, or whenever there is no Ceph cluster provided for our use.

This ceph-cluster is considered to be for development purposes only!

## Prerequisites

Following the instructions below, it is assumed that each of the nodes which will act as a member of the ceph cluster will have an uninitialized, unpartitioned volume attached. You can modify the enclosed yaml files to change this behavior.

> By following the steps below, the ceph nodes will greedily claim all uninitialized volumes on the nodes and use them for ceph.

> If you built the nodes on OpenStack using the `deploy-tools/openstack/build_openstack.sh` script, then your nodes will already have uninitialized 50GB volumes attached.

## Deploying Ceph via Rook

> I repeat, these steps are only required when deploying to OpenStack, or whenever there is no Ceph cluster provided for our use.

```bash
cd ${SVC_HOME}/deploy-tools/openstack/ceph/

# Create the common resources. By default these will be in the rook-ceph namespace.
kubectl create -f common.yaml

# Deploy the rook operator.
kubectl create -f operator.yaml

# Deploy the rook-ceph cluster. This file assumes a raw block storage device is attached.
# Use `cluster-test.yaml` to configure ceph to use directories on the local filesystem.
kubectl create -f cluster.yaml
```

Allow the above commands time to complete. It can take awhile as the volumes are assimilated.

### Verify health of ceph storage provider

This will create a ceph cluster in the `rook-ceph` namespace. Verify that the pods are running with `kubectl get pods -n rook-ceph`.

You can verify the health of the storage provider by creating a tools node from which to run the `ceph` command line:

```bash
kubectl create -f cluster/examples/kubernetes/ceph/toolbox.yaml
kubectl -n rook-ceph exec -it $(kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" -o jsonpath='{.items[0].metadata.name}') bash

# A few basic ceph commmands to get you started...
ceph status
ceph osd status
ceph df
rados df
```

### Create your filesystem

```bash
kubectl create -f filesystem.yaml
```

### Creating the cephfs StorageClass

The files in this repository have been pre-modified to use the standard naming convention used by our production PaaS. Feel free to modify the yaml files if necessary.

```bash
kubectl create -f csi/cephfs/storageclass.yaml
kubectl create -f csi/cephfs/pvc.yaml
```

### Creating the raw block StorageClass

The files in this repository have been pre-modified to use the standard naming convention used by our production PaaS. Feel free to modify the yaml files if necessary.

```bash
kubectl create -f csi/rbd/storageclass.yaml
kubectl create -f csi/rbd/pvc.yaml
```

### Verify your PVCs

The last two commands should have generated a 1GB test private volume claim for the `csi-cephfs-sc` and `csi-rdb-sc` storage classes, respectively.

> Make sure the two newly-created PVCs show the status of `BOUND`

```bash
kubectl get pvc
```

### ceph dashboard

By default, ceph creates a user `admin`, You can retrieve the password:

```bash
kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o jsonpath="{['data']['password']}" | base64 --decode && echo
```

Expose your service:

```bash
kubectl port-forward -n rook-ceph svc/rook-ceph-mgr-dashboard 8443:8443
```

## Troubleshooting

[Common Issues](https://rook.io/docs/rook/v1.2/common-issues.html)

## Rook versions

The yaml files included in the `dev_services` repo come from Rook `release-1.2`.

If you would like to download another release of the files, you can do so with the following:

```bash
git clone --single-branch --branch ${RELEASE} https://github.com/rook/rook.git
```

Where `${RELEASE}` is a valid release branch of the rook repository.