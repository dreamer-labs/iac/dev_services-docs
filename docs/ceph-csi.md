# Ceph

## Create/Retrieve the Ceph client keys

This step must be run from one of the proxmox nodes.

```
ceph auth get-or-create client.kubernetes mon 'profile rbd' osd 'profile rbd pool=PaaS' mgr 'profile rbd pool=PaaS'
ceph auth ls | grep -A 1 client.admin
ceph fsid
```

Save the output of all three commands somewhere. We will be using these values in the nawt container when we add them to vault.

## Deploy Ceph-CSI

> NOTE: Ensure variables are correct in `/dev_services/kubernetes/deploy_ceph_k8s.yaml`. You may need to modify the KUBECONFIG, the IP addresses of the Ceph server nodes, and the ceph_k8s_cluster_id.

From **inside** the nawt container:

```
vault-auto-unseal.sh
vault login <Initial Root Token>
export VAULT_TOKEN=<Initial Root Token>
# The following command will fail if the path already exists. That's okay.
vault secrets enable -path=${ENVIRONMENT}/secrets kv
vault kv put ${ENVIRONMENT}/secrets/ceph/ceph.client.kubernetes key=<CLIENT.KUBERNETES.KEY>
vault kv put ${ENVIRONMENT}/secrets/ceph/ceph.client.admin key=<CLIENT.ADMIN.KEY>
cd /dev_services/kubernetes
ansible-playbook deploy_ceph_k8s.yaml
```

Now verify that the PVCs have been created:

`kubectl describe pvc raw-block-pvc`

`kubectl describe pvc csi-cephfs-pvc`
