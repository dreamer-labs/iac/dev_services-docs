# Building the environment

We do not presently have an automated system for provisioning in Proxmox, but it is in the works.

## Virtual Machines

In the meantime, we will make four full clones of `Virtual Machine 9004 (ubuntu18.04-docker19.03.9) on node proxmox-01` named as follows.

> It may be easier if you make a single clone of image `9004`, configure the hardware as necessary, and then make three clones of this pre-configured instance (this way you only need to configure the cloud-init portion of each subsequent node):

  - kube-01
  - kube-02
  - kube-03
  - rancher

NOTES:

- Distribute/migrate the 3 kube nodes across different proxmox nodes.
- The rancher node can reside on any proxmox node.
- Set the following hardware parameters for each node:

## Hardware Configuration

```text
Memory: 32768MB (32GB)
Processor: 1 socket, 16 cores
Hard disk SCSI0, resize to 50GB
  - Target Storage: vms
Hard disk SCSI1, resize to 25GB
  - Target Storage: vms
Network Device net0:
  - type: virtio
  - bridge: vmbr2
  - vlan: 3005
  - firewall: disabled
Network Device net1:
  - type: virtio
  - bridge: vmbr2
  - vlan 2005
  - firewall: disabled
```

Once you have created one of the nodes, you can set some of the cloud-init options that will be identical across all nodes, such as `user`, `DNS Domain`, `DNS Servers`:

## cloud-init Configuration

```text
User: ubuntu
Password: optional if you want to login from the proxmox console
SSH Public Key: copy the public key from your deploy node

DNS Domain: devservices.int  # Set to your intended search domain
DNS Servers: 10.43.0.10    # This needs to be the valid service ip for coredns of your cluster (you can update this after you deploy with rke once your cluster is up if needed)

IP Config (net0):
  - ip={{ kube_private_ips }}/24
  - gw={{ private_gateway_ip }}
IP Config (net1):
  - ip={{ kube_public_ips }}/24
```

At this point, I like to boot the first node and make sure everything is working and then update only the IP Config for the remaining clones.

## Configure the LOG drive

> This step only needs to happen if you are building the very first base-image (commonly stored with VMID 9004). If you already have a VM pre-configured, SKIP THIS STEP.

We configured a second drive for logs in the steps above. We need to configure the VM to mount that directory automatically.

LOGIN TO THE VM YOU JUST CREATED

```bash
lsblk
# Verify that your 25G drive is sdb
sudo parted /dev/sdb mklabel msdos
sudo parted /dev/sdb mkpart primary 0% 100%
sudo mkfs.ext4 -L LOGS /dev/sdb1

# Update fstab
echo "LABEL=LOGS      /var/log        ext4    defaults        0 0" | sudo tee -a /etc/fstab

# Mount the log drive and verify
sudo mount -a
# You should get no output from that command.

# Verify that /var/log is mounted to /dev/sdb1
df -h | grep log
```

> At this point, I like to save this as a base image with the ID `9004` and the name `ubuntu18.04-docker19.03.9`. Then right-click on it from Proxmox and select `Convert to template`. From this point, any time you rebuild the cluster the you can use image `9004` and it will be pre-configured and only require cloud-init changes.

## Notes

> We have reserved ips `*.101, *.102, and *.103` for our kubectl nodes and `*.105` for our rancher node. Be sure to set the IP Config accordingly for all nodes or manually modify the `inventory.ini` and `cluster_{{ environment }}.yml` and `cluster_RANCHER.yml`!!!

> **IMPORTANT NOTE:** The kubernetes canal CNI requires a gateway be configured on the private (net0) interface or it will fail to create the virtual network interfaces and installation will fail.

> **NOTE:** Currently you can specify a gateway on `net1` of the development environment as `10.20.5.254` if you need internet access for troubleshooting.
