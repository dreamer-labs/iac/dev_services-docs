# Gitlab-ce

This repository contains the scripts, documentation, and Helm chart required to deploy and manage Gitlab-CE on Kubernetes.

## Installation

```bash
# Create namespace
export GITLAB_NS=gitlab-{{ environment }}
kubectl create ns ${GITLAB_NS} # Only if you don't have a namespace for gitlab already

# Generate some random passwords and store them in the vault
vault kv put ${ENVIRONMENT}/secrets/gitlab-ce postgresql-password=$(openssl rand -base64 16) repmgr-password=$(openssl rand -base64 16)

# Create postgres secret
kubectl create secret generic postgres-secrets -n ${GITLAB_NS} \
  --from-literal=postgresql-password=$(vault kv get --format=json ${ENVIRONMENT}/secrets/gitlab-ce | jq -r '.data.data["postgresql-password"]') \
  --from-literal=repmgr-password=$(vault kv get --format=json ${ENVIRONMENT}/secrets/gitlab-ce | jq -r '.data.data["repmgr-password"]')

# Create SSL secret
mkdir -p /dev_services/_certs/gitlab/
vault write ${ENVIRONMENT}/pki_int/issue/devservices_int \
  common_name={{ hostname }}.{{ domain }} \
  alt_names=
  {%- for d in alt_domains -%}
         {{ hostname }}.{{ d }},
  {%- endfor -%}{{ hostname}}.devservices.int \
  ttl=${TLS_TTL} \
   --format=json > /dev_services/_certs/gitlab/certs-combined.json

jq -r '.data.certificate' /dev_services/_certs/gitlab/certs-combined.json > /dev_services/_certs/gitlab/tls.crt
jq -r '.data.issuing_ca' /dev_services/_certs/gitlab/certs-combined.json > /dev_services/_certs/gitlab/ca.crt
jq -r '.data.private_key' /dev_services/_certs/gitlab/certs-combined.json > /dev_services/_certs/gitlab/tls.key

kubectl create secret generic gitlab-ssl-certs --from-file=ca.crt=/dev_services/_certs/gitlab/ca.crt --from-file=tls.key=/dev_services/_certs/gitlab/tls.key --from-file=tls.crt=/dev_services/_certs/gitlab/tls.crt -n ${GITLAB_NS}

> Update the values.yaml file to change things like the IP address of the load balancer.

# Deploy the helm chart. Only run this step if you are deploying outside of the final environment and do not have 
# SSH HOST KEYS or TRUSTED CERTS. Once you are inside the final environment, continue to the next steps.
helm upgrade -i gitlab gitlab-ce-omnibus \
     -n ${GITLAB_NS} \
     --set core.ssh_host_keys.enabled=false \
     --set core.trusted_certs.enabled=false \
     --set core.ldap_backend.enabled=false \
     --set expose.loadBalancer.IP={{ endpoint }} \
     --set core.env.GITLAB_EXTERNAL_URL=https://{{ hostname }}.{{ domain }} \
     --set core.env.GITLAB_SSH_HOST={{ hostname }}.{{ domain }} \
     --set core.env.GITLAB_PAGES_EXTERNAL_URL={{ pages_url }} \
     -f values.yaml


# Store our currently deployed helm values file in the vault:
vault kv patch ${ENVIRONMENT}/secrets/gitlab-ce values.yaml=@<(helm get values -n ${GITLAB_NS} gitlab)
```

## Custom SSH Host Keys

This helm chart has support for custom SSH host keys.

> The `ssh_ed25519_host_key` and `ssh_ecdsa_host_key` files need to be downloaded from `/etc/gitlab` on the  legacy gitlab server. This ensures that existing runners will continue to trust ssh connections to our gitlab server.

```bash
# Create a secret from the retrieved host_key files.
kubectl create secret ssh-host-keys -n ${GITLAB_NS} --from-file=ssh_ed25519_host_key --from-file=ssh_ecdsa_host_key --from-file=ssh_rsa_host_key

# Modify values file OR set on command line
# NOTE: These values are specified true by default in values.yaml
helm upgrade -i gitlab -n ${GITLAB_NS} gitlab-ce-omnibus --set core.ssh_host_keys.enabled=true --set core.ssh_host_keys.secretName=ssh-host-keys -f values.yaml

helm upgrade -i gitlab gitlab-ce-omnibus \
     -n ${GITLAB_NS} \
     --set core.ssh_host_keys.enabled=true \
     --set core.ssh_host_keys.secretName=ssh-host-keys \
     --set core.trusted_certs.enabled=false \
     --set core.ldap_backend.enabled=false \
     --set expose.tls.loadBalancer.IP={{ endpoint }} \
     --set core.env.GITLAB_EXTERNAL_URL=https://{{ hostname }}.{{ domain }} \
     --set core.env.GITLAB_SSH_HOST={{ hostname }}.{{ domain }} \
     --set core.env.GITLAB_PAGES_EXTERNAL_URL={{ pages_url }} \
     -f values.yaml

# Store our currently deployed helm values file in the vault:
vault kv patch ${ENVIRONMENT}/secrets/gitlab-ce values.yaml=@<(helm get values -n ${GITLAB_NS} gitlab)
```

## Custom Trusted CA Certs

This helm chart also has support for custom trusted CA certs. If you need Gitlab itself to trust a self-signed CA do the following:

```bash
# Create a secret containing your CA cert
kubectl create secret generic trusted-certs -n ${GITLAB_NS} --from-file=cust-root-ca.crt=${ROOT_CA}

# Create a secret for ldap configuration
kubectl create secret generic ldap-bind-info -n ${GITLAB_NS} --literal=bind_dn=<BIND_DN_VALUE> --from-literal=password=<BIND_PASSWORD>

helm upgrade -i gitlab gitlab-ce-omnibus \
     -n ${GITLAB_NS} \
     --set core.ssh_host_keys.enabled=true \
     --set core.ssh_host_keys.secretName=ssh-host-keys \
     --set core.trusted_certs.enabled=true \
     --set core.ldap_backend.enabled=true \
     --set expose.tls.loadBalancer.IP={{ endpoint }} \
     --set core.env.GITLAB_EXTERNAL_URL=https://{{ hostname }}.{{ domain }} \
     --set core.env.GITLAB_SSH_HOST={{ hostname }}.{{ domain }} \
     --set core.env.GITLAB_PAGES_EXTERNAL_URL={{ pages_url }} \
     -f values.yaml

# Store our currently deployed helm values file in the vault:
vault kv patch ${ENVIRONMENT}/secrets/gitlab-ce values.yaml=@<(helm get values -n ${GITLAB_NS} gitlab)
```

## Backups

Backups are taken using a Kubernetes Cron Job. This Cron Job will spin up a pod which exec's into the gitlab-core container and runs the Gitlab Omnibus backup command. These backups are placed in `/var/opt/gitlab/backups`, which is a persistent volume.

The backup interval can be defined in the `values.yaml` file. By default the backup is taken daily at 12:00 server time.

### Restoration

#### Requirements

You will need the gitlab-secrets.json file and the gitlab backup file.
Additionally you will need to make sure you gitlab.rb configuration file in the helm chart is the same as your current Gitlab's configuration file. This can be modified in gitlab-ce-omnibus/conf/gitlab.rb

#### Procedure

In order to restore from a backup you must first disable the Kubernetes Liveness probes. If you do not disable these checks then Kubernetes will restart the gitlab-core pod during the restoration.

```bash
# Set the failureThreshold of the livenessProbe to 10000 so that the pod doesn't get re-created during the restore.
kubectl patch statefulset \
        gitlab-gitlab-git-core  \
        -n ${GITLAB_NS} \
        --type merge \
        --patch '{"spec":{"template":{"spec":{"containers":[{"livenessProbe":{"failureThreshold": 10000}}]}}}}'
```

The gitlab pod will restart. Wait for the gitlab pod to come back up.

Copy the backup file to the gitlab pod. 

```bash
kubectl cp -n ${GITLAB_NS} <backup file> gitlab-gitlab-git-core-0:/var/opt/gitlab/backups

# Exec into the pod.
kubectl exec -n ${GITLAB_NS} -ti gitlab-gitlab-git-core-0 /bin/bash

# Inside the pod, run:
chown git.git /var/opt/gitlab/backups/*.tar

gitlab-ctl stop unicorn && gitlab-ctl stop sidekiq
```

We have now stopped the services and the clock is ticking until the pod gets automatically recreated for failure.

Execute the restore:

> The gitlab-backup command assumes '_gitlab_backup.tar' extension, so we have to remove that from the filename.

For example, find the most recent backup tar file and remove _gitlab_backup.tar from the name.

```bash
# The following will return the most-recent backup file. Feel free to specify the file of your choice.
LATEST_BACKUP_FILE=$(ls -t /var/opt/gitlab/backups/*.tar)
BACKUP_PATH=${LATEST_BACKUP_FILE//_gitlab_backup.tar}

# BACKUP_PATH should now point to the file with the extraneous part removed.

gitlab-backup restore BACKUP=${BACKUP_PATH} force=yes 2>&1 > /tmp/restore_logs &

# Exit the container
exit
```

Tail the logs so we can safely monitor the process. We exited the pod because otherwise our connection will timeout, stopping the backup in mid-process

```bash
kubectl exec -it -n ${GITLAB_NS} gitlab-gitlab-git-core-0 -- bash -c "tail -f /tmp/restore_logs"
```

When the restore completes, copy in the `gitlab-secrets.json` file from the legacy production server:

```bash
kubectl cp gitlab-secrets.json gitlab-gitlab-git-core-0:/etc/gitlab -n ${GITLAB_NS}
```

Restart and reconfigure gitlab:

```bash
# Exec into the container.
kubectl exec -it -n ${GITLAB_NS} gitlab-gitlab-git-core-0 /bin/bash

# Inside the container, run the following:
gitlab-ctl reconfigure
gitlab-ctl restart
gitlab-rake gitlab:check SANITIZE=true
exit
```

Change the failureThreshold back to 10:

```bash
kubectl patch statefulset \
        gitlab-gitlab-git-core  \
        -n ${GITLAB_NS} \
        --type merge \
        --patch '{"spec":{"template":{"spec":{"containers":[{"livenessProbe":{"failureThreshold": 10}}]}}}}'
```

### Fixing Permissions

`gitlab-rake` may report permissions issues on the rails upload directory. If so, complete the following from inside the gitlab-core container:

```bash
kubectl exec -n ${GITLAB_NS} -ti gitlab-gitlab-git-core-0 /bin/bash

chown -R git /var/opt/gitlab/gitlab-rails/uploads
find /var/opt/gitlab/gitlab-rails/uploads -type f -exec chmod 0644 {} \;
find /var/opt/gitlab/gitlab-rails/uploads -type d -not -path /var/opt/gitlab/gitlab-rails/uploads -exec chmod 0700 {} \;
```

## Resetting the root password

We don't have the root password to login to legacy production gitlab. We can reset it once we have migrated all of the data to our new gitlab server:

> The root password that we use is stored in vault inside the nawt container: `vault kv get ${ENVIRONMENT}/secrets/gitlab-ce`

```bash
kubectl exec -n ${GITLAB_NS} -ti gitlab-gitlab-git-core-0 /bin/bash

# Inside the container
gitlab-rails console -e production

user = User.where(id: 1).first

user.password = '<PASSWORD_FROM_VAULT>'

user.password_confirmation = '<PASSWORD_FROM_VAULT>'

user.save!
```

Full documentation: https://docs.gitlab.com/ce/security/reset_root_password.html

## Giving an LDAP user admin permissions

Login to the git server and become root.

```bash
kubectl exec -ti -n ${GITLAB_NS} gitlab-core-0 /bin/bash

# Inside the container
gitlab-rails console -e production

user = User.find_by_email("username@email.net")

user.admin = true

user.save!
```

## Putting Gitlab into ReadOnly mode

Putting Gitlab in ReadOnly mode will disable write operations to Gitlab. This includes project modifications, code pushes, and runners. It is a good idea to put Gitlab into ReadOnly mode before starting a maintenance.

Edit /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/database.rb
On line 58 change the function definition to true

```yaml
def self.read_only?
  false
end
```

Reconfigure gitlab:

```bash
kubectl exec -n ${GITLAB_NS} -ti gitlab-gitlab-git-core-0 /bin/bash

# Inside the container:
gitlab-ctl reconfigure

gitlab-ctl restart
```

## Registering a new gitlab-runner

It is recommended that you register a new gitlab runner during pre-flight:

```bash
# needed if you want to keep your configuration.
docker volume create gitlab-runner-config

# gitlab/gitlab-runner should be available in harbor
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```

You'll need two things before you continue:
1. A copy of the TLS ca-bundle. This can be downloaded from portal.{{ name }}.net.
2. A runner token from gitlab, which can be generated per-project or per-group in the settings tab.

Run the following:

```bash
docker exec -ti gitlab-runner /bin/bash

# You'll need to copy your ca-bundle into the container: docker cp, copy paste, whatever works.
gitlab-runner register --tls-ca-file=/<path_to_ca_bundle>
```

Use the following settings when prompted:
name = "temporary-runner-from-container"
tags = "gitlab-deployment"
url = "https://git.{{ name }}.net/"
token = "<token found from gitlab page>"
executor = "shell"

Now you'll need to restart your docker container:

```bash
docker restart gitlab-runner
```

Your gitlab runner is now registered and can begin picking up jobs based on the tag(s) you provided!

### Installing the gitlab-runner package

If for any reason you need to install gitlab-runner onto a VM, the deb packages are available at:
http://mirror.{{ name }}.net/gitlab/apt/mirror/packages.gitlab.com/runner/gitlab-runner/ubuntu/pool/bionic/main/g/gitlab-runer

> The URL above is for ubuntu 18.04. Navigate up the directory structure to find the packages for other releases.

## Building Gitlab Pages

Pages are built via `.gitlab-ci.yml`. We have provided a working example which has already been deployed to: https://dream.{{ name }}.io/pages-deployment-test

However, if you need to build your own deployment test, you can easily create a new project in gitlab and create it from a template. Choose the Gitlab plain-html template.

To publish your own repository, create a directory called `public/` that contains everything you want published to pages, and create a `.gitlab-ci.yml` file similar to the following:

```yaml
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```
