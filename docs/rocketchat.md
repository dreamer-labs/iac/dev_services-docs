# Rocketchat

```bash
export ROCKETCHAT_NS=rocketchat-{{ environment }}

mkdir -p /dev_services/_certs/rocketchat/

vault write ${ENVIRONMENT}/pki_int/issue/devservices_int \
    common_name={{ hostname }}.{{ domain }} \
    alt_names=
    {%- for d in alt_domains -%}
           {{ hostname }}.{{ d }},
    {%- endfor -%}{{ hostname }}.devservices.int \
    ttl=${TLS_TTL} \
    --format=json \
    > /dev_services/_certs/rocketchat/certs-combined.json

jq -r '.data.certificate' /dev_services/_certs/rocketchat/certs-combined.json > /dev_services/_certs/rocketchat/tls.crt
jq -r '.data.issuing_ca' /dev_services/_certs/rocketchat/certs-combined.json > /dev_services/_certs/rocketchat/ca.crt
jq -r '.data.private_key' /dev_services/_certs/rocketchat/certs-combined.json > /dev_services/_certs/rocketchat/tls.key

# create the namespace if it does not exist
kubectl create namespace ${ROCKETCHAT_NS}

# save the resulting certs as tls.crt, tls.key, and ca.crt
kubectl create secret generic rocketchat-tls-secret \
    -n ${ROCKETCHAT_NS} \
    --from-file=/dev_services/_certs/rocketchat/tls.key \
    --from-file=/dev_services/_certs/rocketchat/tls.crt \
    --from-file=/dev_services/_certs/rocketchat/ca.crt

# store the int-ca as a secret:
kubectl create secret generic int-ca -n ${ROCKETCHAT_NS} --from-file=int_ca.pem=${ROOT_CA}

vault kv put ${ENVIRONMENT}/secrets/rocketchat \
  mongodbPassword="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)" \
  mongodbRootPassword="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)"

helm upgrade -i rocketchat rocketchat \
  -n ${ROCKETCHAT_NS} \
  --set ingress-nginx.controller.service.loadBalancerIP={{ endpoint }} \
  --set host={{ hosntame }}.{{ domain }} \
  --set ingress.hosts[0]=rocketchat.{{ domain }} \
  --set ingress.hosts[1]=rocketchat.staging.{{ domain }} \
  --set ingress.hosts[2]=rocketchat.prod.{{ domain }} \
  --set ingress.tls[0].hosts[0]=rocketchat.{{ domain }} \
  --set ingress.tls[0].hosts[1]=rocketchat.staging.{{ domain }} \
  --set ingress.tls[0].hosts[2]=rocketchat.prod.{{ domain }} \
  --set mongodb.mongodbPassword=$(vault kv get --format json ${ENVIRONMENT}/secrets/rocketchat | jq -r '.data.data["mongodbPassword"]') \
  --set mongodb.mongodbRootPassword=$(vault kv get --format json ${ENVIRONMENT}/secrets/rocketchat | jq -r '.data.data["mongodbRootPassword"]') \
  -f values.yaml

# Store our currently deployed helm values file in the vault:
vault kv patch ${ENVIRONMENT}/secrets/rocketchat values.yaml=@<(helm get values -n ${ROCKETCHAT_NS} rocketchat)
```

## Setting the IP address
This rocketchat helm chart will install an ingress-nginx ingress controller in the release namespace.
You can set the IP address of this ingress controller by setting the following value:
`ingress-nginx.controller.service.loadBalancerIP`

This value is also available in the values file.

## Manual config

### Setup General Settings
goto:
  Administration -> General: set "Site URL" to `https://rocketchat.<deployment_url>`

### Setup Gitlab OAuth

1. Ensure General Settings Site URL point to your deployment URL


2. Setup Gitlab OAuth config

goto
  `Gitlab deployment: https://gitlab.<deployment_url>` -> Admin -> Applications -> Add New Application

```
Name: rocketchat OR rocketchat-dev OR etc.
Callback URL: https://rocketchat.<deployment_url>/_oauth/gitlab
Trusted: 	Y
Confidential: 	Y
Scopes:

    api (Access the authenticated user's API)
    read_user (Read the authenticated user's personal information)
    read_api (Read Api)
    openid (Authenticate using OpenID Connect)
    profile (Allows read-only access to the user's personal information using OpenID Connect)
    email (Allows read-only access to the user's primary email address using OpenID Connect)

```

goto
  Adminstration -> OAuth: > Gitlab: set

```
Oauth Enabled: true
GitLab URL: https://gitlab.<deployment_url>
Gitlab Id: "Application ID" created in Gitlab for rocketchat ex: 7dc90a3b70e92d98a8bd257c800b87f
Client Secret: 44f72cb94db8edd9
Identity Path: /api/v4/user
merge users: enable
```

## Disable Default Login Form

### Make sure you do this to avoid confusion on login

Administration -> Accounts -> Show Default Login Form

## Enable login form

```bash
curl http://localhost:3000/api/v1/login  -d "username=USERNAME&password=PASSWORD"
// get you user-id and auth-token

curl -H "X-Auth-Token: LOGIN-TOKEN"  -H "X-User-Id: SOME-ID" -H "Content-Type: application/json"  http://localhost:3000/api/v1/settings/Accounts_ShowFormLogin  -d '{"value": true}'
```