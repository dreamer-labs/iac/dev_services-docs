# Generating certificates

In this section we will configure vault to generate our TLS certificates.

## Preparing vault

We have provided a simple pre-populated hashicorp vault which contains some basic secrets that can be used during a deployment. For a development build you will want to import this vault.

### Importing the vault template

```bash
# We configured the SVC_HOME and VAULT_ARCHIVE variables in the beginning
cd ${SVC_HOME}
mkdir -p ${SVC_HOME}/deploy-tools/volumes
sudo chown -R ubuntu: ${SVC_HOME}/deploy-tools/volumes
./vault/scripts/export-vault.sh --import ${VAULT_ARCHIVE}
```

This will decompress the encrypted vault volume into a directory that can be read by the nawt container.

### Unsealing the vault

For our dev environment, we have included a handy-dandy script to automatically unseal the vault.

From **inside** the nawt container, run:

```bash
nawt # Enter our nawt container

# This will unseal the vault and append the VAULT_TOKEN to our environment variables.
vault-auto-unseal.sh | tee >(grep 'Root Token' | awk '{print $4}' | sed -e 's/^/export VAULT_TOKEN=/' >> $HOME/.paas/vault-token)
. $HOME/.paas/vault-token
vault login ${VAULT_TOKEN}
```

## Enabling kv secrets engine in vault.

```bash
export ENVIRONMENT={{ environment }}
vault secrets enable -path=${ENVIRONMENT}/secrets kv-v2
```

> VAULT_TOKEN is now set permanently on both the host machine and the nawt container.

## Enabling PKI in vault

We have pre-populated PKI within the vault for ${ENVIRONMENT}=dev. If you are building a new new environment (i.e. staging/prod) you will need to execute the following steps, changing ENVIRONMENT to suit your needs.

> Skip this step if you wish to use the dev certificates already pre-populated

> Skip this step if you will be importing an existing CA-bundle for the target environment.

```bash
nawt

# Configure vault to assign TLS certificates
export ENVIRONMENT={{ environment }}
MAX_TTL=$(expr ${TLS_TTL%?} +  ${TLS_TTL%?})h

vault secrets enable -path=${ENVIRONMENT}/pki_int pki
vault secrets tune -max-lease-ttl=${MAX_TTL} ${ENVIRONMENT}/pki_int

# Create a role that is authorized to issue certificates
vault write ${ENVIRONMENT}/pki_int/roles/devservices_int \
    allowed_domains=docker.io,{{ domain }},devservices.int,dldev.xyz \
    allow_ip_sans=true \
    allow_subdomains=true \
    max_ttl=${MAX_TTL} \
    ttl=${MAX_TTL}
```

### Generate a new ROOT CA

```bash
nawt

# Generate a root certificate
export ENVIRONMENT={{ environment }}
vault write ${ENVIRONMENT}/pki_int/root/generate/internal \
    common_name={{ domain }} \
    ttl=${MAX_TTL}

# TODO: We need to store this cert at $ROOT_CA
```
> Before you continue, copy the certificates output from the previous command into ${SVC_HOME}/_certs/cacerts.pem

```bash
vault write ${ENVIRONMENT}/pki_int/config/urls \
    issuing_certificates="http://127.0.0.1:8200/v1/${ENVIRONMENT}/pki/ca" \
    crl_distribution_points="http://127.0.0.1:8200/v1/${ENVIRONMENT}/pki/crl"

# Issue a test certificate. common_name can be any valid domain defined above
vault write ${ENVIRONMENT}/pki_int/issue/devservices_int \
    common_name=dev.dldev.xyz \
    ttl=${TLS_TTL}
```

Update your ROOT_CA!

```bash
# EXIT THE NAWT CONTAINER
export ROOT_CA=${SVC_HOME}/_certs/cacerts.pem

# re-source the env.sh to lock in this new value
. ${SVC_HOME}/env.sh

# Update your certificates bundle
sudo cp ${ROOT_CA} /usr/local/share/ca-certificates/root-ca.crt
sudo update-ca-certificates --fresh
```

> If you have created your own CA this way, you are finished with this document. Continue to `Deploying the Registry`

### Setup PKI with existing ROOT or Intermediate CA

Ensure the variable ROOT_CA points to your root CA crt. As it is used for setup scripts and later install steps.

```bash
nawt

export ROOT_CA=${SVC_HOME}/ca-tools/demo-ca/root-ca.crt

OUR_INTERMEDIATE_CA_UNENCRYPTED_KEY=our-intermediate-ca-unencrypted.key

OUR_SIGNED_INTERMEDIATE_CA=/dev_services/ca-tools/demo-ca/int-ca.crt

ROOT_CA=${ROOT_CA:-/dev_services/ca-tools/demo-ca/root-ca.crt}
```

## Create a secrets engine and load an intermediate or root CA into vault

### Prep an encrypted private key for the pem bundle

If the private key is stored encrypted the file will begin with `-----BEGIN ENCRYPTED PRIVATE KEY-----`

```bash
nawt
OUR_INTERMEDIATE_CA_ENCRYPTED_KEY=/dev_services/ca-tools/demo-ca/int-ca.key

openssl rsa -in ${OUR_INTERMEDIATE_CA_ENCRYPTED_KEY} -out ${OUR_INTERMEDIATE_CA_UNENCRYPTED_KEY}
```

### Prep and upload the key/cert pem bundle

```bash
nawt

cat ${OUR_INTERMEDIATE_CA_UNENCRYPTED_KEY} ${OUR_SIGNED_INTERMEDIATE_CA} > cated_pem_cacert_priv_key.pem
vault write ${ENVIRONMENT}/pki_int/config/ca pem_bundle=@cated_pem_cacert_priv_key.pem
```

### Rotating vault credentials for a new environment

It may be necessary to rotate the vault unseal and root tokens

Follow these to rotate creds:

- [ops-rekeying-and-rotating](https://learn.hashicorp.com/vault/operations/ops-rekeying-and-rotating)
- [ops-generate-root](https://learn.hashicorp.com/vault/operations/ops-generate-root)

Update the resulting unseal keys and root token in /persist/last-dev.txt

## Test Validate certificates

### Add root CA to your local system

> **NOTE** Only needed on systems you intend to validate/connect to services using these certs. For many internal services, the root CA should be present. You will need the root CA pem cert file used to sign the intermediate, not the intermediate pem cert for this step.

Assuming you do not have have the appropriate root CA add the root to your local system:

```bash
sudo cp ${ROOT_CA} /usr/local/share/ca-certificates/root-ca.crt
sudo update-ca-certificates --fresh
```

### Test the Intermediate CA cert against our root CA

Now run the test of the intermediate... (having you do this so we know the system trusts the intermediate before testing the generated certs)

```bash
openssl verify ${OUR_SIGNED_INTERMEDIATE_CA}

our-intermediate-ca.crt: OK
```

### Test generated signed key/cert pair

Generate a key pair

```bash
nawt
cd /tmp
vault write ${ENVIRONMENT}/pki_int/issue/devservices_int \
  common_name=somefqdn.{{ domain }} \
  alt_names=another.{{ domain }} \
  ttl=6h \
  --format=json > certs-combined.json

jq -r '.data.certificate' certs-combined.json > tls.crt
jq -r '.data.issuing_ca' certs-combined.json > cacert.pem
jq -r '.data.private_key' certs-combined.json > tls.key

# Create a cert chain from the above output
cat tls.crt cacert.pem > tls-chain.pem
```

## Retrieve a certificate stored in vault

In this example, we will retrieve a certificate for our local registry from vault.

From **inside** our nawt container, we have a vault which contains the certs:

```bash
cd /dev_services/rancher
mkdir -p certs
vault-auto-unseal
vault login <Initial Root Token>
vault kv get --field tls.key dev/secrets/registry/certs > certs/tls.key
vault kv get --field tls.crt dev/secrets/registry/certs > certs/tls.crt
vault kv get --field cacerts.pem dev/secrets/registry/certs > certs/cacerts.pem
vault kv get --field ca-key.pem dev/secrets/registry/certs > certs/ca-key.pem
```
