.. test documentation master file, created by
   sphinx-quickstart on Thu Aug 20 01:51:16 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

dev services documentation
==========================

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Contents:

   README.rst
   getting-started.rst
   environment.rst
   vault-pki.rst
   registry.rst
   rke.rst
   ceph-rook.rst
   ceph-csi.rst
   metallb.rst
   gitlab-ce.rst
   harbor.rst
   rocketchat.rst
   rancher.rst
   vault-cluster.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
